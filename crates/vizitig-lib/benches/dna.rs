use criterion::*;
use vizitig_lib::dna::DNA;
use rand::{Rng, SeedableRng};
use rand_chacha;

const NUCLEO : [&str; 4]= ["A", "C", "G", "T"];

fn generate_dna<const N:usize>() -> String{
    let mut rng = rand_chacha::ChaCha8Rng::seed_from_u64(42);
    let arr : Vec<String> = (0 .. N).map(|_|{
        NUCLEO[(rng.random::<u8>() as usize) % 4].into()
    }).collect();
    arr.join("")
}


pub fn criterion_benchmark(c: &mut Criterion){
    let to_convert = generate_dna::<1000>();
    c.bench_function("small_dna (1000 bases) from str",|b| b.iter(||{
        let dna: DNA = to_convert.as_str().try_into().unwrap();
        black_box(dna);
    }));

    let to_convert = generate_dna::<1000_000>();
    c.bench_function("medium_dna (1M bases) from str",|b| b.iter(||{
        let dna: DNA = to_convert.as_str().try_into().unwrap();
        black_box(dna);
    }));
    let to_convert = generate_dna::<10_000_000>();
    c.bench_function("medium_dna (10M bases) from str",|b| b.iter(||{
        let dna: DNA = to_convert.as_str().try_into().unwrap();
        black_box(dna);
    }));

    let to_convert = generate_dna::<100_000_000>();
    c.bench_function("large_dna (100M bases) from str",|b| b.iter(||{
        let dna: DNA = to_convert.as_str().try_into().unwrap();
        black_box(dna);
    }));
}
criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
