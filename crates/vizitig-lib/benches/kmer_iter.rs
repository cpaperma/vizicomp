use criterion::*;
use vizitig_lib::dna::DNA;
use vizitig_lib::iterators::{KmerIterator, CanonicalKmerIterator};
use vizitig_lib::kmer_simd::SimdDataStore;
use rand::{Rng, SeedableRng};
use rand_chacha;
use paste::paste;

const NUCLEO : [&str; 4]= ["A", "C", "G", "T"];

fn generate_dna<const N:usize>() -> String{
    let mut rng = rand_chacha::ChaCha8Rng::seed_from_u64(42);
    let arr : Vec<String> = (0 .. N).map(|_|{
        NUCLEO[(rng.random::<u8>() as usize) % 4].into()
    }).collect();
    arr.join("")
}


pub fn criterion_benchmark(c: &mut Criterion){
macro_rules! gen_bench{
    ($iter: ty, $k: expr, $store:ty, $($size:expr)*) => {
        paste!{
            {
                $(
                 let dna: DNA = generate_dna::<$size>().as_bytes().try_into().unwrap();
                 c.bench_function(&format!("find min kmer on {} {} ({} base)", stringify!($iter), $k, $size).to_string() ,|b| b.iter(||{
                     let iter: $iter::<$k, $store> = (&dna).into();
                     black_box(iter.min());
                 }));
                )*
            }
        }
    }
}

    gen_bench!(KmerIterator, 31, u64, 1000_0000 10_000_0000 100_0000_000);
    gen_bench!(CanonicalKmerIterator, 31, u64, 1000_0000 10_000_0000 100_0000_000);
    gen_bench!(KmerIterator, 31, u128, 1000_0000 10_000_0000 100_0000_000);
    gen_bench!(CanonicalKmerIterator, 31, u128, 1000_0000 10_000_0000 100_0000_000);
    gen_bench!(CanonicalKmerIterator, 97, SimdDataStore<97>, 1000_0000 10_000_0000 100_0000_000);

}
criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
