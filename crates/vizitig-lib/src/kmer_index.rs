//! Uses [`Mmap`](memmap2) to map a file into memory with kernel support.
//!
//! Choose this implementation if it is supported by your architecture.
//! Should work on Linux and other Unix base systems. memmap2 claim
//! to be multiplateform but to check on windows.
//!
//! The underlying implementation of the index is based on:
//! - rayon parallel sorting (to build the index)
//! - binary_search of slice (to fetch data)
//!
//! This should not require a lot of RAM except for the buffer to write some file at
//! index creation.

use crate::dna::DNA;
use crate::error::VizitigErr;
use crate::iterators::CanonicalKmerIterator;
use crate::kmer::{DataStore, Kmer};
use memmap2::{Mmap, MmapMut};
use rayon::prelude::*;
use std::cmp::Ordering;
use std::fmt;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::path::Path;
use std::sync::Arc;

#[derive(Clone, Debug)]
pub struct DNAIndexEntry<'a, V: Clone> {
    pub dna: &'a DNA,
    pub val: V,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Copy, Clone, Debug)]
pub struct IndexEntry<K: Ord + Copy + fmt::Debug, V: Ord + Copy + fmt::Debug> {
    pub key: K,
    pub val: V,
}

impl<K: Ord + Copy + fmt::Debug + fmt::Display, V: Ord + Copy + fmt::Debug + fmt::Display>
    fmt::Display for IndexEntry<K, V>
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.key, self.val)?;
        Ok(())
    }
}

fn dump_raw_data<K: Ord + Copy + fmt::Debug, V: Ord + Copy + fmt::Debug>(
    iterator: impl Iterator<Item = IndexEntry<K, V>>,
    out_path: &Path,
    buffer_size: usize,
) {
    let file_out = File::options()
        .create(true)
        .append(true)
        .open(out_path)
        .unwrap();
    let mut buff_out = BufWriter::with_capacity(buffer_size, file_out);
    iterator.for_each(|u| {
        // convert a raw
        unsafe {
            let data: &[u8] = std::slice::from_raw_parts(
                &u as *const IndexEntry<K, V> as *const u8,
                std::mem::size_of::<IndexEntry<K, V>>(),
            );
            buff_out.write_all(data).unwrap();
        }
    });
    buff_out.flush().unwrap();
}

fn dump_raw_data_dna<
    'a,
    const N: usize,
    T: DataStore<N> + Copy + fmt::Debug + Ord,
    V: Copy + fmt::Debug + Ord,
>(
    iterator: impl Iterator<Item = DNAIndexEntry<'a, V>>,
    out_path: &Path,
    buffer_size: usize,
) {
    let kmer_iterator = iterator.flat_map(|entry| {
        let kmer_it: CanonicalKmerIterator<N, T> = entry.dna.into();
        let val = entry.val;
        kmer_it.map(move |u| IndexEntry::<Kmer<N, T>, V> { key: u, val })
    });
    dump_raw_data::<Kmer<N, T>, V>(kmer_iterator, out_path, buffer_size);
}

#[derive(Clone)]
pub struct Index<K: Copy + fmt::Debug + Ord + 'static, V: Ord + Copy + fmt::Debug + 'static> {
    sorted_slice: &'static [IndexEntry<K, V>],
    pub mmap: Arc<Mmap>,
}

impl<K: Copy + fmt::Debug + Ord + Send, V: Ord + Copy + fmt::Debug + Send> Index<K, V> {
    unsafe fn _build_index(out_path: &Path) -> Result<Self, VizitigErr> {
        let file_out = File::options()
            .read(true)
            .write(true)
            .open(out_path)
            .unwrap();
        let mut raw_mmap = MmapMut::map_mut(&file_out).unwrap();
        let kmerentry_size = std::mem::size_of::<IndexEntry<K, V>>();
        let n: usize = raw_mmap.len() / kmerentry_size;
        let data = raw_mmap.as_mut_ptr();
        let slice: &mut [IndexEntry<K, V>] =
            std::slice::from_raw_parts_mut(data as *mut IndexEntry<K, V>, n);
        slice.par_sort_unstable();
        let mut shift: usize = 0;
        for i in 1..n {
            if slice[i] == slice[i - shift - 1] {
                shift += 1;
            } else if shift > 0 {
                slice[i - shift] = slice[i];
            }
        }
        let _ = file_out.set_len(((n - shift) * kmerentry_size).try_into().unwrap()); // truncate the file
        Ok(Self::load_index(out_path).unwrap())
    }

    pub fn build_index(
        iterator: impl Iterator<Item = IndexEntry<K, V>>,
        out_path: &Path,
        buffer_size: usize,
    ) -> Result<Self, VizitigErr> {
        dump_raw_data::<K, V>(iterator, out_path, buffer_size);
        unsafe { Self::_build_index(out_path) }
    }

    pub fn load_index(path: &Path) -> Result<Self, VizitigErr> {
        let file = File::options().read(true).open(path).unwrap();

        unsafe {
            match Mmap::map(&file) {
                Ok(mmap) => {
                    let n: usize = mmap.len() / std::mem::size_of::<IndexEntry<K, V>>();
                    let data = mmap.as_ptr();
                    let slice: &[IndexEntry<K, V>] =
                        std::slice::from_raw_parts(data as *const IndexEntry<K, V>, n);
                    Ok(Index::<K, V> {
                        sorted_slice: slice,
                        mmap: Arc::from(mmap),
                    })
                }
                _ => Err(VizitigErr::InputError),
            }
        }
    }

    pub fn get(&self, key: K) -> Result<V, VizitigErr> {
        self.get_pos(self.find_pos(key)?)
    }

    // return the left most position with the value K
    pub fn find_pos(&self, key: K) -> Result<usize, VizitigErr> {
        let result = self
            .sorted_slice
            .binary_search_by_key(&key, |&entry| entry.key);
        match result {
            Ok(mut res) => {
                loop {
                    if res == 0 {
                        break;
                    }
                    if self.sorted_slice[res].key != key {
                        break;
                    }
                    res -= 1;
                }
                if self.sorted_slice[res].key != key {
                    res += 1;
                }
                Ok(res)
            }
            Err(_) => Err(VizitigErr::IndexKeyError),
        }
    }

    pub fn get_pos(&self, pos: usize) -> Result<V, VizitigErr> {
        if pos < self.len() {
            return Ok(self.sorted_slice[pos].val);
        }
        Err(VizitigErr::IndexKeyError)
    }

    pub fn get_all(&self, key: K) -> Result<impl Iterator<Item = V>, VizitigErr> {
        let left_most = self.find_pos(key)?;
        let iterator = IndexIterator {
            index: self.clone(),
            offset: left_most,
        };

        Ok(iterator
            .take_while(move |entry| entry.key == key)
            .map(|entry| entry.val))
    }

    pub fn join_iter<V2: Ord + Copy + fmt::Debug + Send>(
        &self,
        iterator: impl Iterator<Item = (K, V2)>,
        out_path: &Path,
        buffer_size: usize,
    ) -> Index<V, V2> {
        Index::<V, V2>::build_index(
            iterator
                .filter_map(|(key, val2)| match self.get_all(key) {
                    Ok(iter) => Some(iter.map(move |val| IndexEntry::<V, V2> {
                        key: val,
                        val: val2,
                    })),
                    _ => None,
                })
                .flatten(),
            out_path,
            buffer_size,
        )
        .unwrap()
    }

    pub fn intersection_iter(
        &self,
        iterator: impl Iterator<Item = K>,
        out_path: &Path,
        buffer_size: usize,
    ) -> Index<V, ()> {
        self.join_iter(iterator.map(|k| (k, ())), out_path, buffer_size)
    }

    pub fn join_index<V2: Ord + Copy + fmt::Debug + Send>(
        &self,
        other: Index<K, V2>,
        out_path: &Path,
        buffer_size: usize,
    ) -> Index<V, V2> {
        let file_out = File::options()
            .create(true)
            .append(true)
            .open(out_path)
            .unwrap();
        let mut buff_out = BufWriter::with_capacity(buffer_size, file_out);
        let mut index_self: usize = 0;
        let mut index_other: usize = 0;
        let self_size: usize = self.sorted_slice.len();
        let other_size: usize = other.sorted_slice.len();
        while index_self < self_size && index_other < other_size {
            let self_val = self.sorted_slice[index_self];
            let other_val = other.sorted_slice[index_other];
            match self_val.key.cmp(&other_val.key) {
                Ordering::Equal => {
                    let val = IndexEntry::<V, V2> {
                        key: self_val.val,
                        val: other_val.val,
                    };
                    unsafe {
                        let data: &[u8] = std::slice::from_raw_parts(
                            &val as *const IndexEntry<V, V2> as *const u8,
                            std::mem::size_of::<IndexEntry<V, V2>>(),
                        );
                        buff_out.write_all(data).unwrap();
                    }
                    index_other += 1;
                }
                Ordering::Less => {
                    // we need to enumerate the whole cartesian product
                    // when the key of self and other matches.
                    index_self += 1;
                    if index_self == self_size {
                        break;
                    }
                    if self.sorted_slice[index_self].key == self_val.key && index_other > 0 {
                        while other.sorted_slice[index_other - 1].key == self_val.key {
                            index_other -= 1;
                            if index_other == 0 {
                                break;
                            }
                        }
                    }
                }
                _ => {
                    index_other += 1;
                }
            }
        }
        unsafe {
            buff_out.flush().unwrap();
            Index::<V, V2>::_build_index(out_path).unwrap()
        }
    }

    pub fn is_empty(&self) -> bool {
        self.sorted_slice.is_empty()
    }

    pub fn len(&self) -> usize {
        self.sorted_slice.len()
    }
}

pub struct IndexIterator<K: Copy + fmt::Debug + Ord + 'static, V: Copy + fmt::Debug + Ord + 'static>
{
    index: Index<K, V>,
    offset: usize,
}

impl<K: Copy + fmt::Debug + Ord + Send, V: Copy + fmt::Debug + Ord + Send> Iterator
    for IndexIterator<K, V>
{
    type Item = IndexEntry<K, V>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.offset >= self.index.sorted_slice.len() {
            None
        } else {
            let res = self.index.sorted_slice[self.offset];
            self.offset += 1;
            Some(res)
        }
    }
}

impl<K: Copy + fmt::Debug + Ord + Send, V: Copy + fmt::Debug + Ord + Send> IntoIterator
    for Index<K, V>
{
    type Item = IndexEntry<K, V>;
    type IntoIter = IndexIterator<K, V>;
    fn into_iter(self) -> Self::IntoIter {
        IndexIterator::<K, V> {
            index: self,
            offset: 0,
        }
    }
}

pub type KmerIndex<const N: usize, T, V> = Index<Kmer<N, T>, V>;
pub type KmerIndexEntry<const N: usize, T, V> = IndexEntry<Kmer<N, T>, V>;

impl<
        'a,
        const N: usize,
        T: DataStore<N> + Ord + Copy + fmt::Debug + Send,
        V: Ord + Copy + fmt::Debug + Send,
    > KmerIndex<N, T, V>
{
    ///  Build the index from an iterator of DNA
    ///
    ///  # Safety
    ///
    ///  The underlying code uses mmap files which are inherently unsafe.
    ///  see mmap2 crate for more details.
    pub unsafe fn build_index_dna(
        iterator: impl Iterator<Item = DNAIndexEntry<'a, V>>,
        out_path: &Path,
        buffer_size: usize,
    ) -> Result<Self, VizitigErr> {
        dump_raw_data_dna::<N, T, V>(iterator, out_path, buffer_size);
        unsafe { Self::_build_index(out_path) }
    }
}

#[cfg(test)]
mod tests_index {
    use super::*;
    use tempfile::NamedTempFile;

    #[test]
    fn test_intersection_iter() {
        let file1 = NamedTempFile::new().unwrap().into_temp_path();
        let fileout = NamedTempFile::new().unwrap().into_temp_path();
        let items: Vec<IndexEntry<usize, usize>> = [2, 3, 5, 7, 11, 13]
            .into_iter()
            .enumerate()
            .map(|(i, p)| IndexEntry { key: p, val: i })
            .collect();
        let index = Index::<usize, usize>::build_index(items.into_iter(), &file1, 1000).unwrap();

        assert_eq!(6, index.len());
        assert_eq!(2, index.get(5).unwrap());
        let res_index =
            index.intersection_iter([0, 1, 2, 2, 2, 1, 2, 11].into_iter(), &fileout, 1000);
        let res_values: Vec<IndexEntry<usize, ()>> = res_index.into_iter().collect();
        let res_expected: Vec<IndexEntry<usize, ()>> = vec![
            IndexEntry { key: 0, val: () },
            IndexEntry { key: 4, val: () },
        ];
        assert_eq!(res_expected, res_values);
    }

    #[test]
    fn test_join_index() {
        let file1 = NamedTempFile::new().unwrap().into_temp_path();
        let file2 = NamedTempFile::new().unwrap().into_temp_path();
        let fileout = NamedTempFile::new().unwrap().into_temp_path();
        let items: Vec<IndexEntry<usize, usize>> = [2, 3, 5, 5, 11, 13]
            .into_iter()
            .enumerate()
            .map(|(i, p)| IndexEntry { key: p, val: i })
            .collect();
        let index = Index::<usize, usize>::build_index(items.into_iter(), &file1, 1000).unwrap();

        let items2: Vec<IndexEntry<usize, usize>> = [2, 4, 7, 5, 5, 13, 15]
            .into_iter()
            .enumerate()
            .map(|(i, j)| IndexEntry { key: j, val: i })
            .collect();
        let index2 = Index::<usize, usize>::build_index(items2.into_iter(), &file2, 1000).unwrap();

        let res_index = index.join_index(index2, &fileout, 1000);
        let res_values: Vec<IndexEntry<usize, usize>> = res_index.into_iter().collect();
        let res_expected: Vec<IndexEntry<usize, usize>> = vec![
            IndexEntry { key: 0, val: 0 },
            IndexEntry { key: 2, val: 3 },
            IndexEntry { key: 2, val: 4 },
            IndexEntry { key: 3, val: 3 },
            IndexEntry { key: 3, val: 4 },
            IndexEntry { key: 5, val: 5 },
        ];
        assert_eq!(res_expected, res_values);
    }

    #[test]
    fn test_kmer_index() {
        let file = NamedTempFile::new().unwrap();
        let path = file.into_temp_path();
        let dnas: Vec<DNA> = ["AAAAC", "ACACACACACACA", "ACGC"]
            .into_iter()
            .map(|s| s.as_bytes().try_into().unwrap())
            .collect();

        let items: Vec<DNAIndexEntry<usize>> = dnas
            .iter()
            .enumerate()
            .map(|(i, dna)| DNAIndexEntry { dna, val: i.pow(2) })
            .collect();
        let kmer_index;
        unsafe {
            kmer_index =
                KmerIndex::<4, u64, usize>::build_index_dna(items.clone().into_iter(), &path, 1000)
                    .unwrap();
        }

        assert_eq!(5, kmer_index.len());

        let kmer0: Kmer<4, u64> = "AAAA".try_into().unwrap();
        assert_eq!(0, kmer_index.get(kmer0).unwrap());

        let kmer1: Kmer<4, u64> = "ACAC".try_into().unwrap();
        assert_eq!(1, kmer_index.get(kmer1).unwrap());

        let kmer2: Kmer<4, u64> = "ACGC".try_into().unwrap();
        assert_eq!(4, kmer_index.get(kmer2).unwrap());

        let kmer3: Kmer<4, u64> = "CCCC".try_into().unwrap();
        assert_eq!(Err(VizitigErr::IndexKeyError), kmer_index.get(kmer3));

        let mut items: Vec<KmerIndexEntry<4, u64, usize>> = kmer_index.into_iter().collect();
        items.sort();
        let res: Vec<KmerIndexEntry<4, u64, usize>> = vec![
            KmerIndexEntry {
                key: "AAAA".as_bytes().try_into().unwrap(),
                val: 0,
            },
            KmerIndexEntry {
                key: "AAAC".as_bytes().try_into().unwrap(),
                val: 0,
            },
            KmerIndexEntry {
                key: "ACAC".as_bytes().try_into().unwrap(),
                val: 1,
            },
            KmerIndexEntry {
                key: "ACGC".as_bytes().try_into().unwrap(),
                val: 4,
            },
            KmerIndexEntry {
                key: "CACA".as_bytes().try_into().unwrap(),
                val: 1,
            },
        ];
        assert_eq!(res, items);
    }

    #[test]
    fn test_kmer_index_set() {
        let file = NamedTempFile::new().unwrap();
        let path = file.into_temp_path();
        let dnas: Vec<DNA> = ["AAAAC", "ACACACACACACA", "ACGC"]
            .into_iter()
            .map(|s| s.as_bytes().try_into().unwrap())
            .collect();

        let items: Vec<DNAIndexEntry<()>> = dnas
            .iter()
            .map(|dna| DNAIndexEntry { dna, val: () })
            .collect();
        let kmer_index;
        unsafe {
            kmer_index =
                KmerIndex::<4, u64, ()>::build_index_dna(items.clone().into_iter(), &path, 1000)
                    .unwrap();
        }

        assert_eq!(5, kmer_index.len());

        let kmer0: Kmer<4, u64> = "AAAA".try_into().unwrap();
        assert_eq!((), kmer_index.get(kmer0).unwrap());

        let kmer1: Kmer<4, u64> = "ACAC".try_into().unwrap();
        assert_eq!((), kmer_index.get(kmer1).unwrap());

        let kmer2: Kmer<4, u64> = "ACGC".try_into().unwrap();
        assert_eq!((), kmer_index.get(kmer2).unwrap());

        let kmer3: Kmer<4, u64> = "CCCC".try_into().unwrap();
        assert_eq!(Err(VizitigErr::IndexKeyError), kmer_index.get(kmer3));

        let mut items: Vec<KmerIndexEntry<4, u64, ()>> = kmer_index.into_iter().collect();
        items.sort();
        let res: Vec<KmerIndexEntry<4, u64, ()>> = vec![
            IndexEntry {
                key: "AAAA".as_bytes().try_into().unwrap(),
                val: (),
            },
            IndexEntry {
                key: "AAAC".as_bytes().try_into().unwrap(),
                val: (),
            },
            IndexEntry {
                key: "ACAC".as_bytes().try_into().unwrap(),
                val: (),
            },
            IndexEntry {
                key: "ACGC".as_bytes().try_into().unwrap(),
                val: (),
            },
            IndexEntry {
                key: "CACA".as_bytes().try_into().unwrap(),
                val: (),
            },
        ];
        assert_eq!(res, items);
    }

    #[test]
    fn test_kmer_multiset_semantic() {
        let file = NamedTempFile::new().unwrap();
        let path = file.into_temp_path();
        let dnas: Vec<DNA> = ["ACGTACGT", "ACGTACGT", "ACGCACGT"]
            .into_iter()
            .map(|s| s.as_bytes().try_into().unwrap())
            .collect();

        let items: Vec<DNAIndexEntry<usize>> = dnas
            .iter()
            .enumerate()
            .map(|(i, dna)| DNAIndexEntry { dna, val: i.pow(2) })
            .collect();
        let kmer_index;
        unsafe {
            kmer_index =
                KmerIndex::<4, u64, usize>::build_index_dna(items.clone().into_iter(), &path, 1000)
                    .unwrap();
        }
        let kmer: Kmer<4, u64> = "ACGT".try_into().unwrap();
        let result: Vec<usize> = kmer_index.get_all(kmer).unwrap().collect();
        assert_eq!(result, vec![0, 1, 4]);
    }
}
