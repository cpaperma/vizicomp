use crate::dna::{Nucleotid, DNA};
use crate::kmer::{DataStore, Kmer};
use std::cmp::min;
use std::slice::Iter;

pub struct KmerIterator<'a, const N: usize, T: DataStore<N> + Copy + Ord> {
    initialized: bool,
    it: Iter<'a, Nucleotid>,
    current_kmer: Option<Kmer<N, T>>,
}

impl<const N: usize, T: DataStore<N> + Copy + Ord> Iterator for KmerIterator<'_, N, T> {
    type Item = Kmer<N, T>;

    #[inline(always)]
    fn next(&mut self) -> Option<Self::Item> {
        if !self.initialized {
            self.initialized = true;
            return self.current_kmer;
        }
        let nucleotid = *(self.it.next()?);
        self.current_kmer = Some(self.current_kmer?.append(nucleotid));
        self.current_kmer
    }
}

impl<'a, const N: usize, T: DataStore<N> + Copy + Ord> From<&'a DNA> for KmerIterator<'a, N, T> {
    fn from(value: &'a DNA) -> Self {
        if value.0.len() >= N {
            let first: &[Nucleotid; N] = value.0.first_chunk::<N>().unwrap();
            return KmerIterator::<N, T> {
                it: (*(value.0.get(N..).unwrap())).iter(),
                current_kmer: Some(first.into()),
                initialized: false,
            };
        }
        KmerIterator::<N, T> {
            it: value.0.get(0..).unwrap().iter(),
            current_kmer: None,
            initialized: false,
        }
    }
}

pub struct CanonicalKmerIterator<'a, const N: usize, T: DataStore<N> + Copy + Ord> {
    it: Iter<'a, Nucleotid>,
    initialized: bool,
    current_kmer: Option<Kmer<N, T>>,
    rc_current_kmer: Option<Kmer<N, T>>,
}

impl<const N: usize, T: DataStore<N> + Copy + Ord> Iterator for CanonicalKmerIterator<'_, N, T> {
    type Item = Kmer<N, T>;

    #[inline(always)]
    fn next(&mut self) -> Option<Self::Item> {
        match self.current_kmer {
            Some(kmer) => {
                if !self.initialized {
                    self.initialized = true;
                    return Some(min(self.rc_current_kmer?, kmer));
                }
                let nucleotid = *(self.it.next()?);
                self.current_kmer = Some(self.current_kmer?.append(nucleotid));
                self.rc_current_kmer =
                    Some(self.rc_current_kmer?.append_left(nucleotid.complement()));
                Some(min(self.current_kmer?, self.rc_current_kmer?))
            }
            None => None,
        }
    }
}

impl<'a, const N: usize, T: DataStore<N> + Copy + Ord> From<&'a DNA>
    for CanonicalKmerIterator<'a, N, T>
{
    fn from(value: &'a DNA) -> Self {
        if value.0.len() >= N {
            let first: &[Nucleotid; N] = value.0.first_chunk::<N>().unwrap();
            let current_kmer: Kmer<N, T> = first.into();
            let rc_current_kmer = current_kmer.rc();
            return CanonicalKmerIterator::<N, T> {
                it: (*(value.0.get(N..).unwrap())).iter(),
                current_kmer: Some(current_kmer),
                rc_current_kmer: Some(rc_current_kmer),
                initialized: false,
            };
        }
        CanonicalKmerIterator::<N, T> {
            it: value.0.get(0..).unwrap().iter(),
            current_kmer: None,
            rc_current_kmer: None,
            initialized: true,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn dna_to_iterator() {
        let dna_seq_empty: &DNA = &("CGATCAAA".as_bytes().try_into().unwrap());
        let mut k_0: KmerIterator<40, u64> = dna_seq_empty.into();
        assert_eq!(None, k_0.next());

        let dna_seq: &DNA = &("CGATCAAA".as_bytes().try_into().unwrap());
        let mut k: KmerIterator<4, u64> = dna_seq.into();

        assert_eq!("4-mer(CGAT)", format!("{}", k.next().unwrap()));
        assert_eq!("4-mer(GATC)", format!("{}", k.next().unwrap()));
        assert_eq!("4-mer(ATCA)", format!("{}", k.next().unwrap()));
        assert_eq!("4-mer(TCAA)", format!("{}", k.next().unwrap()));
        assert_eq!("4-mer(CAAA)", format!("{}", k.next().unwrap()));
        assert_eq!(None, k.next());
    }
    #[test]
    fn dna_to_iterator2() {
        let dna_seq: &DNA = &("GATAA".as_bytes().try_into().unwrap());
        let mut k: KmerIterator<4, u64> = dna_seq.into();

        assert_eq!("4-mer(GATA)", format!("{}", k.next().unwrap()));
        assert_eq!("4-mer(ATAA)", format!("{}", k.next().unwrap()));
        assert_eq!(None, k.next());
    }

    #[test]
    fn dna_to_iterator3() {
        let dna_seq: &DNA = &("ATGCTAGTCTGATCGATTAGCTTAGCTTAGTCTAGTAGCTAGGCTAGATCGATCGTAGCTGAT"
            .as_bytes()
            .try_into()
            .unwrap());
        let mut k: KmerIterator<63, u128> = dna_seq.into();

        assert_eq!(
            "63-mer(ATGCTAGTCTGATCGATTAGCTTAGCTTAGTCTAGTAGCTAGGCTAGATCGATCGTAGCTGAT)",
            format!("{}", k.next().unwrap())
        );
        assert_eq!(None, k.next());
    }

    #[test]
    fn dna_to_canonical_iterator() {
        let dna_seq: &DNA = &("CGATCAAA".as_bytes().try_into().unwrap());
        let mut k: CanonicalKmerIterator<4, u64> = dna_seq.into();

        assert_eq!("4-mer(ATCG)", format!("{}", k.next().unwrap()));
        assert_eq!("4-mer(GATC)", format!("{}", k.next().unwrap()));
        assert_eq!("4-mer(ATCA)", format!("{}", k.next().unwrap()));
        assert_eq!("4-mer(TCAA)", format!("{}", k.next().unwrap()));
        assert_eq!("4-mer(CAAA)", format!("{}", k.next().unwrap()));
        assert_eq!(None, k.next());
    }
    #[test]
    fn dna_to_canonical_iterator2() {
        let dna_seq: &DNA = &("TTATC".as_bytes().try_into().unwrap());
        let mut k: CanonicalKmerIterator<4, u64> = dna_seq.into();

        assert_eq!("4-mer(ATAA)", format!("{}", k.next().unwrap()));
        assert_eq!("4-mer(GATA)", format!("{}", k.next().unwrap()));
        assert_eq!(None, k.next());
    }
}
