use crate::dna::Nucleotid;
use crate::error::VizitigErr;
use std::cmp::min;
use std::fmt;

pub trait DataStore<const N: usize> {
    fn zero() -> Self;
    fn set(&self, x: Nucleotid, i: usize) -> Self;
    fn get(&self, i: usize) -> Nucleotid;
    fn swap(&self, i: usize, j: usize) -> Self;
    fn append(&self, x: Nucleotid) -> Self;
    fn append_left(&self, x: Nucleotid) -> Self;
    fn complement(&self) -> Self;
    fn into_string(self) -> String;
    fn reverse(&self) -> Self;
    fn hash(&self) -> i64;
}

pub const EVEN_BITS: u64 = 0b0101010101010101010101010101010101010101010101010101010101010101;
pub const ODD_BITS: u64 = 0b1010101010101010101010101010101010101010101010101010101010101010;
pub const EVEN_BITS_128: u128 = ((EVEN_BITS as u128) << 64) | (EVEN_BITS as u128);
pub const ODD_BITS_128: u128 = ((ODD_BITS as u128) << 64) | (ODD_BITS as u128);

impl<const N: usize> DataStore<N> for u64 {
    fn zero() -> Self {
        0
    }

    fn hash(&self) -> i64 {
        *self as i64
    }

    #[inline(always)]
    fn set(&self, x: Nucleotid, i: usize) -> Self {
        let a = self & !(3_u64 << ((2 * (N - 1)) - (2 * i))); // Zeroed the 2ith and 2i+1th bits;
        let shifted: Self = u64::from(x) << ((2 * (N - 1)) - (2 * i));
        a | shifted
    }

    #[inline(always)]
    fn get(&self, i: usize) -> Nucleotid {
        const MASK: u64 = 3;
        ((self >> ((2 * (N - 1)) - (2 * i))) & MASK)
            .try_into()
            .unwrap()
    }

    #[inline(always)]
    fn swap(&self, i: usize, j: usize) -> Self {
        let x = <u64 as DataStore<N>>::get(self, i);
        let y = <u64 as DataStore<N>>::get(self, j);
        let swap1 = <u64 as DataStore<N>>::set(self, y, i);
        <u64 as DataStore<N>>::set(&swap1, x, j)
    }

    #[inline(always)]
    fn complement(&self) -> Self {
        (!self) & ((1 << (2 * N)) - 1)
    }

    #[inline(always)]
    fn reverse(&self) -> Self {
        let rbits = self.reverse_bits() >> (64 - 2 * N);
        ((rbits & EVEN_BITS) << 1) | ((rbits & ODD_BITS) >> 1)

        // 10 10 01 01 &self
        // 10 10 01 01 rbits
        // 00 00 10 10 (rbits & EVEN_BITS) << 1
        // 01 01 00 00 (rbits & ODD_BITS) >> 1
        // 01 01 10 10 \o/
    }

    #[inline(always)]
    fn append_left(&self, x: Nucleotid) -> Self {
        let y: u64 = x.into();
        let shiftleft = ({ *self } >> 2) | (y << (2 * (N - 1)));
        shiftleft & ((1 << (2 * N)) - 1)
    }

    #[inline(always)]
    fn append(&self, x: Nucleotid) -> Self {
        let y: u64 = x.into();
        let shift_right = ({ *self } << 2) | y;
        shift_right & ((1 << (2 * N)) - 1)
    }
    fn into_string(self) -> String {
        let desc =
            (0..N).map(|i| <Nucleotid as Into<char>>::into(<u64 as DataStore<N>>::get(&self, i)));
        desc.collect()
    }
}

impl<const N: usize> DataStore<N> for u128 {
    fn zero() -> Self {
        0
    }

    #[inline(always)]
    fn hash(&self) -> i64 {
        ((*self >> 32) as i64) ^ (*self as i64)
    }

    #[inline(always)]
    fn set(&self, x: Nucleotid, i: usize) -> Self {
        let a = self & !(3_u128 << ((2 * (N - 1)) - (2 * i))); // Zeroed the 2ith and 2i+1th bits;
        let shifted: Self = (u64::from(x) as u128) << ((2 * (N - 1)) - (2 * i));
        a | shifted
    }

    #[inline(always)]
    fn get(&self, i: usize) -> Nucleotid {
        const MASK: u128 = 3;
        (((self >> ((2 * (N - 1)) - (2 * i))) & MASK) as u64)
            .try_into()
            .unwrap()
    }

    #[inline(always)]
    fn swap(&self, i: usize, j: usize) -> Self {
        let x = <u128 as DataStore<N>>::get(self, i);
        let y = <u128 as DataStore<N>>::get(self, j);
        let swap1 = <u128 as DataStore<N>>::set(self, y, i);
        <u128 as DataStore<N>>::set(&swap1, x, j)
    }

    #[inline(always)]
    fn complement(&self) -> Self {
        (!self) & ((1 << (2 * N)) - 1)
    }

    #[inline(always)]
    fn reverse(&self) -> Self {
        let rbits = self.reverse_bits() >> (128 - 2 * N);
        ((rbits & EVEN_BITS_128) << 1) | ((rbits & ODD_BITS_128) >> 1)
    }

    #[inline(always)]
    fn append_left(&self, x: Nucleotid) -> Self {
        let y: u128 = u64::from(x) as u128;
        let shiftleft = ({ *self } >> 2) | (y << (2 * (N - 1)));
        shiftleft & ((1 << (2 * N)) - 1)
    }

    #[inline(always)]
    fn append(&self, x: Nucleotid) -> Self {
        let y: u128 = u64::from(x) as u128;
        let shift_right = ({ *self } << 2) | y;
        shift_right & ((1 << (2 * N)) - 1)
    }
    fn into_string(self) -> String {
        let desc =
            (0..N).map(|i| <Nucleotid as Into<char>>::into(<u128 as DataStore<N>>::get(&self, i)));
        desc.collect()
    }
}

#[derive(Eq, PartialEq, PartialOrd, Ord, Copy, Clone, Debug)]
pub struct Kmer<const N: usize, T: DataStore<N> + Ord>(pub T);

impl<const N: usize, T: DataStore<N> + Ord + Copy> Kmer<N, T> {
    #[inline(always)]
    pub fn zero() -> Self {
        Self(T::zero())
    }

    #[inline(always)]
    pub fn hash(&self) -> i64 {
        self.0.hash()
    }

    #[inline(always)]
    pub fn append(&self, x: Nucleotid) -> Self {
        Self(self.0.append(x))
    }

    #[inline(always)]
    pub fn append_left(&self, x: Nucleotid) -> Self {
        Self(self.0.append_left(x))
    }

    #[inline(always)]
    pub fn swap(self, i: usize, j: usize) -> Self {
        Self(self.0.swap(i, j))
    }

    #[inline(always)]
    pub fn complement(self) -> Self {
        Self(self.0.complement())
    }

    #[inline(always)]
    pub fn reverse(self) -> Self {
        Self(self.0.reverse())
    }

    #[inline(always)]
    pub fn rc(self) -> Self {
        Self(self.0.reverse().complement())
    }

    #[inline(always)]
    pub fn normalize(self) -> Self {
        min(self, self.rc())
    }

    #[inline(always)]
    pub fn get_data(self) -> T {
        self.0
    }
}

impl<const N: usize, T: DataStore<N> + Ord> From<&[Nucleotid; N]> for Kmer<N, T> {
    #[inline(always)]
    fn from(seq: &[Nucleotid; N]) -> Self {
        let mut acc = T::zero();
        for (i, x) in seq.iter().enumerate() {
            acc = acc.set(*x, i);
        }
        Self(acc)
    }
}

impl<const N: usize, T: DataStore<N> + Ord> From<T> for Kmer<N, T> {
    fn from(data: T) -> Self {
        Kmer::<N, T>(data)
    }
}

impl<const N: usize, T: DataStore<N> + Ord> TryFrom<&[u8; N]> for Kmer<N, T> {
    type Error = VizitigErr;
    #[inline(always)]
    fn try_from(seq: &[u8; N]) -> Result<Self, Self::Error> {
        let nucleotid_vec: [Nucleotid; N] = seq
            .iter()
            .map(|u| (*u).try_into().unwrap())
            .next_chunk()
            .unwrap();
        Ok((&nucleotid_vec).into())
    }
}

impl<const N: usize, T: DataStore<N> + Ord> TryFrom<&[u8]> for Kmer<N, T> {
    type Error = VizitigErr;
    #[inline(always)]
    fn try_from(seq: &[u8]) -> Result<Self, Self::Error> {
        if seq.len() > N {
            panic!("Convert a string of length {} to a {}-mer", seq.len(), N);
        }
        let bound_seq: Result<&[u8; N], _> = seq.try_into();
        match bound_seq {
            Ok(b) => Self::try_from(b),
            _ => Err(Self::Error::TooSmallString(seq.len(), N)),
        }
    }
}

impl<const N: usize, T: DataStore<N> + Ord + Clone> From<&Kmer<N, T>> for String {
    fn from(kmer: &Kmer<N, T>) -> Self {
        kmer.0.clone().into_string()
    }
}

impl<const N: usize, T: DataStore<N> + Ord> TryFrom<&str> for Kmer<N, T> {
    type Error = VizitigErr;
    #[inline(always)]
    fn try_from(seq: &str) -> Result<Self, Self::Error> {
        Ok(seq.as_bytes().try_into().unwrap())
    }
}

impl<const N: usize, T: DataStore<N> + Ord + Clone> fmt::Display for Kmer<N, T>
where
    T: Ord,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}-mer(", N)?;
        let desc: String = self.into();
        write!(f, "{}", desc)?;
        write!(f, ")")?;
        Ok(())
    }
}

pub type ShortKmer<const N: usize> = Kmer<N, u64>;
pub type LongKmer<const N: usize> = Kmer<N, u128>;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn to_str() {
        let kmer_3: ShortKmer<3> = "AAA".try_into().unwrap();
        assert_eq!("3-mer(AAA)", format!("{}", kmer_3));

        let kmer_4: ShortKmer<4> = "CGTA".try_into().unwrap();
        assert_eq!("4-mer(CGTA)", format!("{}", kmer_4));
    }

    #[test]
    fn append() {
        let mut kmer_3: ShortKmer<3> = "AAA".try_into().unwrap();
        assert_eq!("3-mer(AAA)", format!("{}", kmer_3));

        kmer_3 = kmer_3.append(Nucleotid::T);
        assert_eq!("3-mer(AAT)", format!("{}", kmer_3));

        kmer_3 = kmer_3.append(Nucleotid::G);
        assert_eq!("3-mer(ATG)", format!("{}", kmer_3));

        kmer_3 = kmer_3.append(Nucleotid::C);
        assert_eq!("3-mer(TGC)", format!("{}", kmer_3));
    }

    #[test]
    fn append2() {
        //CGATCAAA
        let mut kmer_4: ShortKmer<4> = "CGAT".try_into().unwrap();
        assert_eq!("4-mer(CGAT)", format!("{}", kmer_4));

        kmer_4 = kmer_4.append(Nucleotid::C);
        assert_eq!("4-mer(GATC)", format!("{}", kmer_4));

        kmer_4 = kmer_4.append(Nucleotid::A);
        assert_eq!("4-mer(ATCA)", format!("{}", kmer_4));

        kmer_4 = kmer_4.append(Nucleotid::A);
        assert_eq!("4-mer(TCAA)", format!("{}", kmer_4));

        kmer_4 = kmer_4.append(Nucleotid::A);
        assert_eq!("4-mer(CAAA)", format!("{}", kmer_4));
    }

    #[test]
    fn append3() {
        let mut kmer_4: ShortKmer<4> = "TTAT".try_into().unwrap();
        assert_eq!("4-mer(TTAT)", format!("{}", kmer_4));

        kmer_4 = kmer_4.append(Nucleotid::C);
        assert_eq!("4-mer(TATC)", format!("{}", kmer_4));
    }
    #[test]
    fn append_left() {
        let mut kmer_3: ShortKmer<3> = "AAA".try_into().unwrap();
        assert_eq!("3-mer(AAA)", format!("{}", kmer_3));

        kmer_3 = kmer_3.append_left(Nucleotid::T);
        assert_eq!("3-mer(TAA)", format!("{}", kmer_3));

        kmer_3 = kmer_3.append_left(Nucleotid::G);
        assert_eq!("3-mer(GTA)", format!("{}", kmer_3));

        kmer_3 = kmer_3.append_left(Nucleotid::C);
        assert_eq!("3-mer(CGT)", format!("{}", kmer_3));
    }

    #[test]
    fn append_left2() {
        let mut kmer_4: ShortKmer<4> = "ATAA".try_into().unwrap();
        assert_eq!("4-mer(ATAA)", format!("{}", kmer_4));

        kmer_4 = kmer_4.append_left(Nucleotid::G);
        assert_eq!("4-mer(GATA)", format!("{}", kmer_4));
    }

    #[test]
    fn swap() {
        let kmer_3: ShortKmer<3> = "CGT".try_into().unwrap();
        assert_eq!("3-mer(TGC)", format!("{}", kmer_3.swap(0, 2)));

        let kmer_4: ShortKmer<4> = "CGTA".try_into().unwrap();
        assert_eq!("4-mer(CATG)", format!("{}", kmer_4.swap(1, 3)));
        assert_eq!("4-mer(AGTC)", format!("{}", kmer_4.swap(0, 3)));
    }

    #[test]
    fn reverse() {
        let kmer_3: ShortKmer<3> = "CGT".try_into().unwrap();
        assert_eq!("3-mer(TGC)", format!("{}", kmer_3.reverse()));
    }

    #[test]
    fn rc() {
        let kmer_3: ShortKmer<3> = "CGT".try_into().unwrap();
        assert_eq!("3-mer(ACG)", format!("{}", kmer_3.rc()));

        let kmer_4: ShortKmer<4> = "CGTA".try_into().unwrap();
        assert_eq!("4-mer(TACG)", format!("{}", kmer_4.rc()));
    }

    #[test]
    fn order_kmer() {
        let kmer_a1: ShortKmer<4> = "ACGT".try_into().unwrap();
        let kmer_b1: ShortKmer<4> = "ACGA".try_into().unwrap();
        assert!(kmer_b1 < kmer_a1, "{} < {}", kmer_b1, kmer_a1);

        let kmer_a2: ShortKmer<4> = "CCGT".try_into().unwrap();
        let kmer_b2: ShortKmer<4> = "ACGT".try_into().unwrap();
        assert!(kmer_b2 < kmer_a2, "{} < {}", kmer_b2, kmer_a2);

        let kmer_a3: ShortKmer<4> = "AGGT".try_into().unwrap();
        let kmer_b3: ShortKmer<4> = "ACGT".try_into().unwrap();
        assert!(kmer_b3 < kmer_a3, "{} < {}", kmer_b3, kmer_a3);

        let kmer_a4: ShortKmer<4> = "TAAA".try_into().unwrap();
        let kmer_b4: ShortKmer<4> = "AAAA".try_into().unwrap();
        assert!(kmer_b4 < kmer_a4, "{} < {}", kmer_b4, kmer_a4);
    }

    #[test]
    fn min_kmer() {
        let kmer_a1: ShortKmer<4> = "ACGT".try_into().unwrap();
        let kmer_b1: ShortKmer<4> = "ACGA".try_into().unwrap();
        assert_eq!(min(kmer_b1, kmer_a1), kmer_b1);

        let kmer_a2: ShortKmer<4> = "ATCG".try_into().unwrap();
        let kmer_b2: ShortKmer<4> = "CGAT".try_into().unwrap();
        assert_eq!(min(kmer_a2, kmer_b2), kmer_a2);
    }

    #[test]
    #[should_panic(expected = "Convert a string of length 4 to a 3-mer")]
    fn to_str_panic() {
        let seq_u8: &[u8] = "AAAA".as_bytes();
        let _: ShortKmer<3> = seq_u8.try_into().unwrap();
    }
    // testing long version

    #[test]
    fn long_to_str() {
        let kmer_3: LongKmer<3> = "AAA".try_into().unwrap();
        assert_eq!("3-mer(AAA)", format!("{}", kmer_3));

        let kmer_4: LongKmer<4> = "CGTA".try_into().unwrap();
        assert_eq!("4-mer(CGTA)", format!("{}", kmer_4));
    }

    #[test]
    fn long_append() {
        let mut kmer_3: LongKmer<3> = "AAA".try_into().unwrap();
        assert_eq!("3-mer(AAA)", format!("{}", kmer_3));

        kmer_3 = kmer_3.append(Nucleotid::T);
        assert_eq!("3-mer(AAT)", format!("{}", kmer_3));

        kmer_3 = kmer_3.append(Nucleotid::G);
        assert_eq!("3-mer(ATG)", format!("{}", kmer_3));

        kmer_3 = kmer_3.append(Nucleotid::C);
        assert_eq!("3-mer(TGC)", format!("{}", kmer_3));
    }

    #[test]
    fn long_append2() {
        //CGATCAAA
        let mut kmer_4: LongKmer<4> = "CGAT".try_into().unwrap();
        assert_eq!("4-mer(CGAT)", format!("{}", kmer_4));

        kmer_4 = kmer_4.append(Nucleotid::C);
        assert_eq!("4-mer(GATC)", format!("{}", kmer_4));

        kmer_4 = kmer_4.append(Nucleotid::A);
        assert_eq!("4-mer(ATCA)", format!("{}", kmer_4));

        kmer_4 = kmer_4.append(Nucleotid::A);
        assert_eq!("4-mer(TCAA)", format!("{}", kmer_4));

        kmer_4 = kmer_4.append(Nucleotid::A);
        assert_eq!("4-mer(CAAA)", format!("{}", kmer_4));
    }

    #[test]
    fn long_append3() {
        let mut kmer_4: LongKmer<4> = "TTAT".try_into().unwrap();
        assert_eq!("4-mer(TTAT)", format!("{}", kmer_4));

        kmer_4 = kmer_4.append(Nucleotid::C);
        assert_eq!("4-mer(TATC)", format!("{}", kmer_4));
    }
    #[test]
    fn long_append_left() {
        let mut kmer_3: LongKmer<3> = "AAA".try_into().unwrap();
        assert_eq!("3-mer(AAA)", format!("{}", kmer_3));

        kmer_3 = kmer_3.append_left(Nucleotid::T);
        assert_eq!("3-mer(TAA)", format!("{}", kmer_3));

        kmer_3 = kmer_3.append_left(Nucleotid::G);
        assert_eq!("3-mer(GTA)", format!("{}", kmer_3));

        kmer_3 = kmer_3.append_left(Nucleotid::C);
        assert_eq!("3-mer(CGT)", format!("{}", kmer_3));
    }

    #[test]
    fn long_append_left2() {
        let mut kmer_4: LongKmer<4> = "ATAA".try_into().unwrap();
        assert_eq!("4-mer(ATAA)", format!("{}", kmer_4));

        kmer_4 = kmer_4.append_left(Nucleotid::G);
        assert_eq!("4-mer(GATA)", format!("{}", kmer_4));
    }

    #[test]
    fn long_swap() {
        let kmer_3: LongKmer<3> = "CGT".try_into().unwrap();
        assert_eq!("3-mer(TGC)", format!("{}", kmer_3.swap(0, 2)));

        let kmer_4: LongKmer<4> = "CGTA".try_into().unwrap();
        assert_eq!("4-mer(CATG)", format!("{}", kmer_4.swap(1, 3)));
        assert_eq!("4-mer(AGTC)", format!("{}", kmer_4.swap(0, 3)));
    }

    #[test]
    fn long_rc() {
        let kmer_3: LongKmer<3> = "CGT".try_into().unwrap();
        assert_eq!("3-mer(ACG)", format!("{}", kmer_3.rc()));

        let kmer_4: LongKmer<4> = "CGTA".try_into().unwrap();
        assert_eq!("4-mer(TACG)", format!("{}", kmer_4.rc()));
    }

    #[test]
    fn long_order_kmer() {
        let kmer_a1: LongKmer<4> = "ACGT".try_into().unwrap();
        let kmer_b1: LongKmer<4> = "ACGA".try_into().unwrap();
        assert!(kmer_b1 < kmer_a1, "{} < {}", kmer_b1, kmer_a1);

        let kmer_a2: LongKmer<4> = "CCGT".try_into().unwrap();
        let kmer_b2: LongKmer<4> = "ACGT".try_into().unwrap();
        assert!(kmer_b2 < kmer_a2, "{} < {}", kmer_b2, kmer_a2);

        let kmer_a3: LongKmer<4> = "AGGT".try_into().unwrap();
        let kmer_b3: LongKmer<4> = "ACGT".try_into().unwrap();
        assert!(kmer_b3 < kmer_a3, "{} < {}", kmer_b3, kmer_a3);

        let kmer_a4: LongKmer<4> = "TAAA".try_into().unwrap();
        let kmer_b4: LongKmer<4> = "AAAA".try_into().unwrap();
        assert!(kmer_b4 < kmer_a4, "{} < {}", kmer_b4, kmer_a4);
    }

    #[test]
    fn long_min_kmer() {
        let kmer_a1: LongKmer<4> = "ACGT".try_into().unwrap();
        let kmer_b1: LongKmer<4> = "ACGA".try_into().unwrap();
        assert_eq!(min(kmer_b1, kmer_a1), kmer_b1);

        let kmer_a2: LongKmer<4> = "ATCG".try_into().unwrap();
        let kmer_b2: LongKmer<4> = "CGAT".try_into().unwrap();
        assert_eq!(min(kmer_a2, kmer_b2), kmer_a2);
    }

    #[test]
    #[should_panic(expected = "Convert a string of length 4 to a 3-mer")]
    fn long_to_str_panic() {
        let seq_u8: &[u8] = "AAAA".as_bytes();
        let _: LongKmer<3> = seq_u8.try_into().unwrap();
    }
}
