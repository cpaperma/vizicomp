use crate::dna::Nucleotid;
use crate::kmer::{DataStore, Kmer, EVEN_BITS, ODD_BITS};
/// We store N nucleotid into a vecteur of u64 of size T.
/// Each vector can store 32 nucleotid and we rely on the implementation of
/// DataStore for u64 as basic operations.
///
/// - N must always be between 32*T + 1 and 32*(T+1) or in other word (N - 1) / 32 == T
/// - T must satisfies LaneCount traits as well
use std::simd::prelude::*;
use std::simd::{LaneCount, SupportedLaneCount};

#[derive(Eq, PartialEq, PartialOrd, Ord, Copy, Clone, Debug)]
pub struct SimdDataStore<const N: usize>(pub Simd<u64, { N / 32 + 1 }>)
where
    LaneCount<{ N / 32 + 1 }>: SupportedLaneCount;

const fn mask_array<const N: usize>() -> Simd<u64, { N / 32 + 1 }>
where
    LaneCount<{ N / 32 + 1 }>: SupportedLaneCount,
{
    let mut res = [!0; N / 32 + 1];
    res[res.len() - 1] = (1u64 << (2 * (N % 32))) - 1;
    // the last u64 is of the shape 000...001111...111
    //                        least significant bit  ^
    //
    //
    // with the shift from 0 to 1 at the position
    // 2*(N % 32)
    Simd::from_array(res)
}

const fn const_splat<const N: usize, const K: u64>() -> Simd<u64, { N / 32 + 1 }>
where
    LaneCount<{ N / 32 + 1 }>: SupportedLaneCount,
{
    Simd::from_array([K; N / 32 + 1])
}

impl<const N: usize> SimdDataStore<N>
where
    LaneCount<{ N / 32 + 1 }>: SupportedLaneCount,
{
    const MASK: Simd<u64, { N / 32 + 1 }> = mask_array::<N>();
    const SHIFT2: Simd<u64, { N / 32 + 1 }> = const_splat::<N, 2>();
    const SHIFTLAST: Simd<u64, { N / 32 + 1 }> = const_splat::<N, 62>();
    const EVEN_VECT: Simd<u64, { N / 32 + 1 }> = const_splat::<N, { EVEN_BITS }>();
    const ODD_VECT: Simd<u64, { N / 32 + 1 }> = const_splat::<N, { ODD_BITS }>();
    const LAST_OFFSET: u64 = (N % 32) as u64;
    const VECTOR_SIZE: usize = N / 32 + 1;

    fn splat(val: u64) -> Self {
        let val = Simd::<u64, { N / 32 + 1 }>::splat(val);
        Self(val)
    }
}

impl<const N: usize> DataStore<N> for SimdDataStore<N>
where
    LaneCount<{ N / 32 + 1 }>: SupportedLaneCount,
    [(); N % 32]:,
{
    fn zero() -> Self {
        Self::splat(0)
    }

    fn hash(&self) -> i64 {
        (self.0[0] as i64) ^ (self.0[1]  as i64)
    }

    #[inline(always)]
    fn set(&self, x: Nucleotid, i: usize) -> Self {
        let offset = i / 32;
        let mut new_val = self.0;
        let mut component = new_val[offset];
        let y: u64 = x.into();
        let small_offset: usize = 2 * (i % 32);
        component &= !((3u64) << small_offset); // zeroed the nucleotide at position small_offset
        component |= y << small_offset;
        new_val[offset] = component;
        Self(new_val)
    }

    #[inline(always)]
    fn get(&self, i: usize) -> Nucleotid {
        let offset = i / 32;
        ((self.0[offset] >> (2 * (i % 32))) & 3u64)
            .try_into()
            .unwrap()
    }

    #[inline(always)]
    fn swap(&self, i: usize, j: usize) -> Self {
        let x = self.get(i);
        let y = self.get(j);
        let swap1 = self.set(y, i);
        swap1.set(x, j)
    }

    #[inline(always)]
    fn complement(&self) -> Self {
        Self((!self.0) & Self::MASK)
    }

    #[inline(always)]
    fn reverse(&self) -> Self {
        let mut x = self.0;
        x = x.reverse();
        x = x.reverse_bits();
        let y = x >> (64 - 2 * Self::LAST_OFFSET);
        x = (y | (x.rotate_elements_left::<1>() << (2 * Self::LAST_OFFSET))) & Self::MASK;
        x = ((x & Self::EVEN_VECT) << 1) | ((x & Self::ODD_VECT) >> 1);
        Self(x)
    }

    #[inline(always)]
    fn append_left(&self, x: Nucleotid) -> Self {
        let mut nvec = self.0 << Self::SHIFT2;
        nvec |= self.0.rotate_elements_right::<1>() >> Self::SHIFTLAST;
        let y: u64 = x.into();
        nvec[0] |= y;
        Self(nvec & Self::MASK)
    }

    #[inline(always)]
    fn append(&self, x: Nucleotid) -> Self {
        let mut nvec = self.0 >> Self::SHIFT2;
        nvec |= self.0.rotate_elements_left::<1>() << Self::SHIFTLAST;
        let y: u64 = x.into();
        nvec[N / 32] |= y << (2 * ((Self::LAST_OFFSET) - 1));
        Self(nvec & Self::MASK)
    }

    fn into_string(self) -> String {
        let desc = (0..N).map(|i| <Nucleotid as Into<char>>::into(self.get(i)));
        desc.collect()
    }
}

pub type BigKmer<const N: usize> = Kmer<N, SimdDataStore<N>>;

//
impl<const N: usize> From<[u8; 8 * (N / 32 + 1)]> for BigKmer<N>
where
    LaneCount<{ N / 32 + 1 }>: SupportedLaneCount,
    [(); N % 32]:,
{
    fn from(data: [u8; 8 * (N / 32 + 1)]) -> Self {
        // Unsafe: transmutation of [u8] to [u64]
        unsafe {
            let ptr = data.as_ptr() as *const u64;
            let narray = std::slice::from_raw_parts(ptr, N / 32 + 1)
                .as_chunks_unchecked::<{ N / 32 + 1 }>()[0];
            narray.into()
        }
    }
}
impl<const N: usize> From<[u64; N / 32 + 1]> for BigKmer<N>
where
    LaneCount<{ N / 32 + 1 }>: SupportedLaneCount,
    [(); N % 32]:,
{
    fn from(data: [u64; N / 32 + 1]) -> Self {
        Kmer::<N, SimdDataStore<N>>(SimdDataStore::<N>(Simd::<u64, { N / 32 + 1 }>::from_array(
            data,
        )))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::iterators::KmerIterator;
    use crate::dna::DNA;


    fn splat_str<const L: char, const N: usize>() -> String {
        (0..N).map(|_| L).collect::<String>()
    }

    fn splat_kmer<const L: char, const N: usize>() -> BigKmer<N>
    where
        [(); N / 32 + 1]:,
        [(); N % 32]:,
        LaneCount<{ N / 32 + 1 }>: SupportedLaneCount,
    {
        splat_str::<L, N>().as_str().try_into().unwrap()
    }

    #[test]
    fn to_str() {
        assert_eq!(
            format!("127-mer({})", splat_str::<'T', 127>()),
            format!("{}", splat_kmer::<'T', 127>())
        );
        assert_eq!(
            format!("255-mer({})", splat_str::<'A', 255>()),
            format!("{}", splat_kmer::<'A', 255>())
        );
        assert_eq!(
            format!("511-mer({})", splat_str::<'A', 511>()),
            format!("{}", splat_kmer::<'A', 511>())
        );
        assert_eq!(
            format!("1023-mer({})", splat_str::<'A', 1023>()),
            format!("{}", splat_kmer::<'A', 1023>())
        );
        assert_eq!(
            format!("2031-mer({})", splat_str::<'A', 2031>()),
            format!("{}", splat_kmer::<'A', 2031>())
        );
        assert_eq!(
            format!("127-mer({})", splat_str::<'A', 127>()),
            format!("{}", splat_kmer::<'A', 127>())
        );
        assert_eq!(
            format!("255-mer({})", splat_str::<'T', 255>()),
            format!("{}", splat_kmer::<'T', 255>())
        );
        assert_eq!(
            format!("511-mer({})", splat_str::<'T', 511>()),
            format!("{}", splat_kmer::<'T', 511>())
        );
        assert_eq!(
            format!("1023-mer({})", splat_str::<'T', 1023>()),
            format!("{}", splat_kmer::<'T', 1023>())
        );
        assert_eq!(
            format!("2031-mer({})", splat_str::<'T', 2031>()),
            format!("{}", splat_kmer::<'T', 2031>())
        );
    }

    #[test]
    fn complement() {
        assert_eq!(
            format!("{}", splat_kmer::<'A', 2031>().complement()),
            format!("{}", splat_kmer::<'T', 2031>())
        );
    }

    #[test]
    fn reverse() {
        let mut base_str = splat_str::<'A', 511>();
        let mut base_kmer = splat_kmer::<'A', 511>();
        for i in 0..10000 {
            let b: Nucleotid = ((i as u64) % 4).try_into().unwrap();
            base_str = format!("{}{}", b, base_str);
            base_str.pop();
            base_kmer = base_kmer.append_left(b);
            assert_eq!(
                format!(
                    "511-mer({}) ({})",
                    base_str.chars().rev().collect::<String>(),
                    i
                ),
                format!("{} ({})", base_kmer.reverse(), i)
            );
        }
        let mut base_str = splat_str::<'A', 2029>();
        let mut base_kmer = splat_kmer::<'A', 2029>();
        for i in 0..10000 {
            let b: Nucleotid = ((i as u64) % 4).try_into().unwrap();
            base_str = format!("{}{}", b, base_str);
            base_str.pop();
            base_kmer = base_kmer.append_left(b);
            assert_eq!(
                format!(
                    "2029-mer({}) ({})",
                    base_str.chars().rev().collect::<String>(),
                    i
                ),
                format!("{} ({})", base_kmer.reverse(), i)
            );
        }
    }

    #[test]
    fn enum_kmer(){
        let base_str = "ATGCTAGTCTGATCGATTAGCTTAGCTTAGTCTAGTAGCTAGGCTAGATCGATCGTAGCTGATATGCTAGTCTGATCGATTAGCTTAGCTTAGTCTAGTAGCTAGGCTAGATCGATCGTAGCTGATATGCTAG";
        let base : DNA = base_str.as_bytes().try_into().unwrap();
        let mut k: KmerIterator<127, SimdDataStore<127>> = (&base).into();
        for (i, kmer) in k.enumerate() {
            let kmer_str : String = (&kmer).into();
            assert!(base_str.contains(kmer_str.as_str()), "{} kmer not in {} (i={})", kmer_str, base_str, i);
        }
    }

    #[test]
    fn append2(){
        let base_str : String = "TAGTCTGATCGATTAGCTTAGCTTAGTCTAGTAGCTAGGCTAGATCGATCGTAGCTGATATGCTAGTCTGATCGATTAGCTTAGCTTAGTCTAGTAGCTAGGCTAGATCGATCGTAGCTGATATGCT".to_string();
        let kmer : BigKmer<127> = base_str.as_str().try_into().unwrap();
        let okmer = kmer.append(Nucleotid::A);
        let mut new_str : String = format!("{}{}", base_str, "A");
        new_str.remove(0);
        let fkmer : BigKmer<127> = new_str.as_str().try_into().unwrap();
        assert!(okmer==fkmer, "{} vs {}", okmer, fkmer);
    }

    #[test]
    fn append_left() {
        let mut base_str = splat_str::<'A', 511>();
        let mut base_kmer = splat_kmer::<'A', 511>();
        for i in 0..10000 {
            let b: Nucleotid = ((i as u64) % 4).try_into().unwrap();
            base_str = format!("{}{}", b, base_str);
            base_str.pop();
            base_kmer = base_kmer.append_left(b);
            assert_eq!(
                format!("511-mer({}) ({})", base_str, i),
                format!("{} ({})", base_kmer, i)
            );
        }
        let mut base_str = splat_str::<'A', 2031>();
        let mut base_kmer = splat_kmer::<'A', 2031>();
        for i in 0..10000 {
            let b: Nucleotid = ((i as u64) % 4).try_into().unwrap();
            base_str = format!("{}{}", b, base_str);
            base_str.pop();
            base_kmer = base_kmer.append_left(b);
            assert_eq!(
                format!("2031-mer({}) ({})", base_str, i),
                format!("{} ({})", base_kmer, i)
            );
        }
        let mut base_str = splat_str::<'G', 2017>();
        let mut base_kmer = splat_kmer::<'G', 2017>();
        for i in 0..10000 {
            let b: Nucleotid = ((i as u64) % 4).try_into().unwrap();
            base_str = format!("{}{}", b, base_str);
            base_str.pop();
            base_kmer = base_kmer.append_left(b);
            assert_eq!(
                format!("2017-mer({}) ({})", base_str, i),
                format!("{} ({})", base_kmer, i)
            );
        }
    }

    #[test]
    fn append_right() {
        let mut base_str = splat_str::<'A', 511>();
        let mut base_kmer = splat_kmer::<'A', 511>();
        for i in 0..10000 {
            let b: Nucleotid = ((i as u64) % 4).try_into().unwrap();
            base_str = format!("{}{}", base_str, b);
            base_str.remove(0);
            base_kmer = base_kmer.append(b);
            assert_eq!(
                format!("511-mer({}) ({})", base_str, i),
                format!("{} ({})", base_kmer, i)
            );
        }
        let mut base_str = splat_str::<'A', 2031>();
        let mut base_kmer = splat_kmer::<'A', 2031>();
        for i in 0..10000 {
            let b: Nucleotid = ((i as u64) % 4).try_into().unwrap();
            base_str = format!("{}{}", base_str, b);
            base_str.remove(0);
            base_kmer = base_kmer.append(b);
            assert_eq!(
                format!("2031-mer({}) ({})", base_str, i),
                format!("{} ({})", base_kmer, i)
            );
        }
    }
}
