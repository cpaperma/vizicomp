use crate::error::VizitigErr;
use std::fmt;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Nucleotid {
    A,
    C,
    G,
    T,
}

impl fmt::Display for Nucleotid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Nucleotid::A => write!(f, "A"),
            Nucleotid::G => write!(f, "G"),
            Nucleotid::C => write!(f, "C"),
            Nucleotid::T => write!(f, "T"),
        }
    }
}

impl Nucleotid {
    #[inline(always)]
    pub fn complement(self) -> Nucleotid {
        match self {
            Nucleotid::A => Nucleotid::T,
            Nucleotid::T => Nucleotid::A,
            Nucleotid::G => Nucleotid::C,
            Nucleotid::C => Nucleotid::G,
        }
    }

    #[inline(always)]
    pub fn as_u8(self) -> u8 {
        match self {
            Nucleotid::A => 0,
            Nucleotid::C => 1,
            Nucleotid::G => 2,
            Nucleotid::T => 3,
        }
    }
}

impl From<Nucleotid> for u64 {
    #[inline(always)]
    fn from(value: Nucleotid) -> Self {
        value.as_u8().into()
    }
}

impl From<Nucleotid> for u8 {
    #[inline(always)]
    fn from(value: Nucleotid) -> Self {
        match value {
            Nucleotid::A => b'A',
            Nucleotid::C => b'C',
            Nucleotid::G => b'G',
            Nucleotid::T => b'T',
        }
    }
}

impl From<Nucleotid> for char {
    #[inline(always)]
    fn from(value: Nucleotid) -> Self {
        match value {
            Nucleotid::A => 'A',
            Nucleotid::C => 'C',
            Nucleotid::G => 'G',
            Nucleotid::T => 'T',
        }
    }
}

impl TryFrom<u64> for Nucleotid {
    type Error = VizitigErr;

    #[inline(always)]
    fn try_from(value: u64) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(Nucleotid::A),
            1 => Ok(Nucleotid::C),
            2 => Ok(Nucleotid::G),
            3 => Ok(Nucleotid::T),
            _ => Err(Self::Error::InvalidByte),
        }
    }
}

impl TryFrom<u8> for Nucleotid {
    type Error = VizitigErr;
    #[inline(always)]
    fn try_from(item: u8) -> Result<Self, Self::Error> {
        match item {
            b'A' => Ok(Nucleotid::A),
            b'G' => Ok(Nucleotid::G),
            b'C' => Ok(Nucleotid::C),
            b'T' => Ok(Nucleotid::T),
            b'a' => Ok(Nucleotid::A),
            b'g' => Ok(Nucleotid::G),
            b'c' => Ok(Nucleotid::C),
            b't' => Ok(Nucleotid::T),
            _ => Err(Self::Error::InvalidByte),
        }
    }
}

impl TryFrom<char> for Nucleotid {
    type Error = VizitigErr;
    #[inline(always)]
    fn try_from(item: char) -> Result<Self, Self::Error> {
        match item {
            'A' => Ok(Nucleotid::A),
            'G' => Ok(Nucleotid::G),
            'C' => Ok(Nucleotid::C),
            'T' => Ok(Nucleotid::T),
            'a' => Ok(Nucleotid::A),
            'g' => Ok(Nucleotid::G),
            'c' => Ok(Nucleotid::C),
            't' => Ok(Nucleotid::T),
            _ => Err(Self::Error::InvalidByte),
        }
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct DNA(pub Vec<Nucleotid>);

impl<'a> TryFrom<&'a [u8]> for DNA {
    type Error = VizitigErr;
    fn try_from(value: &'a [u8]) -> Result<Self, Self::Error> {
        Ok(Self(
            value
                .iter()
                .map(|u| <u8 as TryInto<Nucleotid>>::try_into(*u).unwrap())
                .collect(),
        ))
    }
}

impl<'a> TryFrom<&'a str> for DNA {
    type Error = VizitigErr;
    fn try_from(value: &'a str) -> Result<Self, Self::Error>{
        let dna : DNA = value.as_bytes().try_into().unwrap();
        Ok(dna)
    }
}

impl FromIterator<Nucleotid> for DNA {
    fn from_iter<I: IntoIterator<Item = Nucleotid>>(iter: I) -> Self {
        Self(iter.into_iter().collect())
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn to_u8() {
        assert_eq!(b'A', Nucleotid::A.into());
        assert_eq!(b'G', Nucleotid::G.into());
        assert_eq!(b'C', Nucleotid::C.into());
        assert_eq!(b'T', Nucleotid::T.into());
    }

    #[test]
    fn from_u8() {
        assert_eq!(Nucleotid::A, b'A'.try_into().unwrap());
        assert_eq!(Nucleotid::C, b'C'.try_into().unwrap());
        assert_eq!(Nucleotid::G, b'G'.try_into().unwrap());
        assert_eq!(Nucleotid::T, b'T'.try_into().unwrap());
        assert_eq!(Nucleotid::A, b'a'.try_into().unwrap());
        assert_eq!(Nucleotid::C, b'c'.try_into().unwrap());
        assert_eq!(Nucleotid::G, b'g'.try_into().unwrap());
        assert_eq!(Nucleotid::T, b't'.try_into().unwrap());
    }

    #[test]
    fn from_u8_ect() {
        let x: Result<Nucleotid, _> = b'F'.try_into();
        assert_eq!(Err(VizitigErr::InvalidByte), x)
    }

    #[test]
    fn to_char() {
        assert_eq!('A', Nucleotid::A.into());
        assert_eq!('G', Nucleotid::G.into());
        assert_eq!('C', Nucleotid::C.into());
        assert_eq!('T', Nucleotid::T.into());
    }

    #[test]
    fn from_char() {
        assert_eq!(Nucleotid::A, 'A'.try_into().unwrap());
        assert_eq!(Nucleotid::C, 'C'.try_into().unwrap());
        assert_eq!(Nucleotid::G, 'G'.try_into().unwrap());
        assert_eq!(Nucleotid::T, 'T'.try_into().unwrap());
        assert_eq!(Nucleotid::A, 'a'.try_into().unwrap());
        assert_eq!(Nucleotid::C, 'c'.try_into().unwrap());
        assert_eq!(Nucleotid::G, 'g'.try_into().unwrap());
        assert_eq!(Nucleotid::T, 't'.try_into().unwrap());
    }

    #[test]
    fn from_char_error() {
        let x: Result<Nucleotid, _> = 'F'.try_into();
        assert_eq!(Err(VizitigErr::InvalidByte), x)
    }

    #[test]
    fn from_u8_dna() {
        assert_eq!(
            DNA(vec![Nucleotid::A, Nucleotid::C, Nucleotid::A, Nucleotid::G]),
            "ACAG".as_bytes().try_into().unwrap()
        )
    }
}
