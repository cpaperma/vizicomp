use std::error::Error;
use std::fmt;

#[derive(Debug, PartialEq)]
pub enum VizitigErr {
    InvalidByte,
    TooSmallString(usize, usize),
    InputError,
    IndexKeyError,
}

impl fmt::Display for VizitigErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            VizitigErr::InvalidByte => write!(f, "Invalid Byte"),
            VizitigErr::TooSmallString(i, j) => write!(f, "Too small string {} < {}", i, j),
            VizitigErr::InputError => write!(f, "i/o error"),
            VizitigErr::IndexKeyError => write!(f, "Key not found in index"),
        }
    }
}

impl Error for VizitigErr {}
