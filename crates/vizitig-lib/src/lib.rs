#![feature(iter_next_chunk)]
#![feature(portable_simd)]
#![feature(generic_const_exprs)]
#![feature(slice_as_chunks)]
pub mod dna;
pub mod error;
pub mod iterators;
pub mod kmer;
pub mod kmer_index;
pub mod kmer_simd;
